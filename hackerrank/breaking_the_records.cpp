// https://www.hackerrank.com/challenges/breaking-best-and-worst-records/problem
#include <vector>

using namespace std;

vector<int> breakingRecords(vector<int> scores) {
    vector<int> broke_max_min{0, 0};
    int min = scores[0];
    int max = scores[0];

    for(int i = 1; i < scores.size(); i++) {
        if(scores[i] > max) {
            max = scores[i];
            broke_max_min[0]++;
        }
        else if(scores[i] < min) {
            min = scores[i];
            broke_max_min[1]++;
        }
    }
    return broke_max_min;
}