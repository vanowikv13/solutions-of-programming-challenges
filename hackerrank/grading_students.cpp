//...
#include <vector>

using namespace std;

vector<int> gradingStudents(vector<int> grades) {
    for(int i = 0; i < grades.size(); i++) {
        if(grades[i] >= 38 && (grades[i] + 2) % 5 <= 1)
            grades[i] += 2 - ((grades[i] + 2) % 5);
    }
    return grades;
}

//..