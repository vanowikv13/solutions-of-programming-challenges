#include <vector>
#include <iostream>

using namespace std;

void bonAppetit(vector<int> bill, int k, int b) {
    int sum = 0;

    for(int i = 0; i < bill.size(); i++) {
        if(i != k)
            sum += bill[i];
    }

    int shouldPay = sum / 2;

    if(shouldPay == b)
        cout << "Bon Appetit" << endl;
    else
        cout << b - shouldPay << endl;

}
