// https://www.hackerrank.com/challenges/electronics-shop
#include <vector>

using namespace std;

int getMoneySpent(vector<int> keyboards, vector<int> drives, int b) {
    int max_sum = -1;
    for(auto &x : keyboards) {
        for(auto &y : drives) {
            int sum = x+y;
            if(sum > max_sum && sum <= b)
                max_sum = sum;
        }
    }
    return max_sum;
}