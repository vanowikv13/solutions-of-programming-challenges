//...
#include <vector>

using namespace std;

// Complete the aVeryBigSum function below.
long long aVeryBigSum(vector<long> ar) {
    long long sum = 0;
    for (auto x : ar) {
        sum += x;
    }
    return sum;
}

//...