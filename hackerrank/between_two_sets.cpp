// https://www.hackerrank.com/challenges/between-two-sets/problem
#include <vector>

using namespace std;

bool checkIfDividedByArrElements(int num, vector<int> arr) {
    for(auto &x : arr) {
        if(num % x != 0)
            return false;
    }
    return true;
}

bool checkIfNumIsFactorOfArrElements(int num, vector<int> arr) {
    for(auto &x : arr) {
        if(x % num != 0)
            return false;
    }
    return true;
}

int getTotalX(vector<int> a, vector<int> b) {
    int x=0;

    for(int i = a[a.size()-1]; i<=b[0]; i++) {
        if(checkIfDividedByArrElements(i, a) && checkIfNumIsFactorOfArrElements(i, b)) {
            x++;
        }
    }

    return x;
}
