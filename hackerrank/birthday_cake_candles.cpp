//...
#include <map>
#include <iostream>
#include <vector>

using namespace std;

int birthdayCakeCandles(vector<int> candles) {
    map<int, int> candHeightAmount;
    int max = 0;

    for(auto &x : candles) {
        if (x > max)
            max = x;
        if (candHeightAmount.find(x) != candHeightAmount.end())
            candHeightAmount[x]++;
        else
            candHeightAmount[x] = 1;
    }

    return candHeightAmount[max];
}
//...