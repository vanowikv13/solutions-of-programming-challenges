// https://www.hackerrank.com/challenges/apple-and-orange/problem
//...
#include <iostream>
#include <vector>

using namespace std;


// Complete the countApplesAndOranges function below.
void countApplesAndOranges(int s, int t, int a, int b, vector<int> apples, vector<int> oranges) {
    int apl = 0, orn = 0;

    for(auto &x : apples) {
        int p = a + x;
        if(p >= s && p <= t)
            apl++;
    }

    for(auto &x : oranges) {
        int p = b + x;
        if(p >= s && p <= t)
            orn++;
    }

    cout << apl << endl;
    cout << orn << endl;

}

//...