//https://www.hackerrank.com/challenges/cats-and-a-mouse/problem
#include <string>
#include <cmath>

std::string catAndMouse(int a, int b, int c) {
    int a_c = abs(a - c);
    int b_c = abs(b - c);
    if(a_c > b_c)
        return "Cat B";
    else if(a_c == b_c)
        return "Mouse C";
    else
        return "Cat A";
}