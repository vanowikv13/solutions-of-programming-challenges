#include <vector>
#include <map>

using namespace std;

int migratoryBirds(vector<int> arr) {
    map<int,int> type_count;

    for(auto &x : arr) {
        if(type_count.find(x) != type_count.end())
            type_count[x]++;
        else
            type_count[x] = 1;
    }

    int min_type = 0, max_count=0;
    for(auto &x : type_count) {
        if(x.second > max_count) {
            min_type = x.first;
            max_count = x.second;
        } else if(x.second == max_count) {
            if(x.first < min_type)
                min_type = x.first;
        }
    }
    return min_type;
}
