///...
#include <iostream>
#include <vector>

using namespace std;


void miniMaxSum(vector<int> arr) {
    long long totalSum = arr[0];
    long maxVal = arr[0], minVal=arr[0];

    for(auto it = arr.begin() + 1; it != arr.end(); it++) {
        totalSum += *it;

        if(*it > maxVal)
            maxVal = *it;
        else if(*it < minVal)
            minVal = *it;
    }

    cout << totalSum - maxVal << " " << totalSum - minVal << endl;
}

//...