//...
#include <vector>
#include <cmath>

using namespace std;

int diagonalDifference(vector<vector<int>> arr) {
    int ltrd = 0, rtld = 0;

    int n = arr.size() - 1;

    for(int i = 0; i <= n; i++) {
        ltrd += arr[i][i];
        rtld += arr[i][(n - i)];
    }

    return abs(rtld - ltrd);
}

//..