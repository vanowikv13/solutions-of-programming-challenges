unsigned int pageCount(int n, int p) {
    unsigned int pc = p / 2;
    if(n % 2 == 0) {
        if(pc > ((1+ n - p) / 2))
            return ((1+ n - p) / 2);
    } else {
        if(pc > ((n - p) / 2))
                return ((n - p) / 2);
    }
    return pc;
}