#include <vector>
#include <cmath>
#include <iostream>

using namespace std;

vector<vector<vector<int>>> matrix = {{{8, 1, 6}, {3, 5, 7}, {4, 9, 2}},
                                      {{6, 1, 8}, {7, 5, 3}, {2, 9, 4}},
                                      {{4, 9, 2}, {3, 5, 7}, {8, 1, 6}},
                                      {{2, 9, 4}, {7, 5, 3}, {6, 1, 8}},
                                      {{8, 3, 4}, {1, 9, 5}, {6, 7, 2}},
                                      {{4, 3, 8}, {9, 5, 1}, {2, 7, 6}},
                                      {{6, 7, 2}, {1, 5, 9}, {8, 3, 4}},
                                      {{2, 7, 6}, {9, 5, 1}, {4, 3, 8}}};

int formingMagicSquare(vector<vector<int>> x) {
    int min_ = 0x0fffffff;
    for(auto &mat : matrix) {
        int cost = 0;
        for(int i =0; i < mat.size(); i++) {
            for(int j =0; j < mat.size(); j++) {
                cost += abs(x[i][j] - mat[i][j]);
            }
        }
        if(cost < min_)
            min_ = cost;
    }
    return min_;
}

int main()
{
    cout << formingMagicSquare({{5, 3, 4}, {1, 5, 8}, {6, 4, 2}}) << endl; //7
    cout << formingMagicSquare({{4, 9, 2}, {3, 5, 7}, {8, 1, 5}}) << endl; //1
    cout << formingMagicSquare({{4, 8, 2}, {4, 5, 7}, {6, 1, 6}}) << endl; //4
    cout << formingMagicSquare({{4, 5, 8}, {2, 4, 1}, {1, 9, 7}}) << endl; //14
}