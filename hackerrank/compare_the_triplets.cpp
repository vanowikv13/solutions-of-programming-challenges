//...
#include <vector>

using namespace std;

void compareElementsAndAddToArr(vector<int> &vec, int a, int b) {
    if(a > b)
        vec[0]++;
    else if (a < b)
        vec[1]++;
}

vector<int> compareTriplets(vector<int> a, vector<int> b) {
    vector<int> points{0, 0};

    for(int i = 0; i < a.size(); i++) {
        compareElementsAndAddToArr(points, a[i], b[i]);
    }
    return points;
}

//...