// https://www.hackerrank.com/challenges/sock-merchant/problem
#include <vector>
#include <map>

using namespace std;

int sockMerchant(int n, vector<int> ar) {
    map<int,int> socks;
    for(auto &x : ar) {
        if(socks.find(x) != socks.end())
            socks[x]++;
        else
            socks[x] = 1;
    }


    int count_pairs=0;
    for(auto &x : socks) {
        count_pairs += (x.second / 2);
    }
    return count_pairs;
}
