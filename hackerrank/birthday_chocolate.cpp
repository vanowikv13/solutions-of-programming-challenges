// https://www.hackerrank.com/challenges/the-birthday-bar/problem
#include <vector>

using namespace std;

int birthday(vector<int> s, int d, int m) {
    if(m > s.size())
        return 0;

    int sum=0, counter=0, correct=0;

    for(int i = 0; i < s.size(); i++) {
        counter++;
        sum += s[i];
        if(counter == m) {
            if (sum == d)
                correct++;
            counter--;
            sum -= s[i - m + 1];
        }
    }

    return correct;
}
