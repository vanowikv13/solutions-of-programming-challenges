//...
#include <vector>
#include <iostream>
#include <iomanip>

using namespace std;

void plusMinus(vector<int> arr) {
    float positive = 0.0, negative = 0.0, zero = 0.0;

    for (auto x : arr) {
        if (x > 0)
            positive++;
        else if (x < 0)
            negative++;
        else
            zero++;
    }

    cout << setprecision(6) << fixed;
    cout << positive/arr.size() << endl;
    cout << negative/arr.size() << endl;
    cout << zero/arr.size() << endl;
}

//...