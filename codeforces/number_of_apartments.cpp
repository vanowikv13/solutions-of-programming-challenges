#include <bits/stdc++.h>

using namespace std;

int main() {
    size_t t;
    cin >> t;
    for (size_t i = 0; i < t; i++)
    {
        int n;
        cin >> n;
        int sev = 0, fiv = 0, thr = 0;
        int val = n;
        if(val / 7 >= 1.) {
            sev += static_cast<int>(val / 7);
            val -= 7 * sev;
        }

        if(val / 5 >= 1.) {
            fiv += static_cast<int>(val / 5);
            val -= 5* fiv;
        }

        if(val / 3 >= 1.) {
            thr += static_cast<int>(val / 3);
            val -= 3* thr;
        }

        switch(val) {
            case 1:
                if(sev >= 1) {
                    sev-=1;
                    fiv += 1;
                    thr += 1;
                    val = 0;
                }
                else if(fiv >= 1) {
                    fiv -= 1;
                    thr += 2;
                    val = 0;
                }
                else if(thr >= 2) {
                    thr -= 2;
                    sev += 1;
                    val = 0;
                }
                break;
            case 2:
                if(sev >= 1) {
                    sev-=1;
                    thr += 3;
                    val = 0;
                }
                else if(fiv >= 1) {
                    fiv -= 1;
                    sev += 1;
                    val = 0;
                }
                else if(thr >= 1) {
                    thr -= 1;
                    fiv += 1;
                    val = 0;
                }
                break;
            case 4:
                if(sev >= 3) {
                    sev-=3;
                    fiv += 5;
                    val = 0;
                }
                else if(fiv >= 1) {
                    fiv -= 1;
                    thr += 3;
                    val = 0;
                }
                else if(thr >= 1) {
                    thr -= 1;
                    sev += 1;
                    val = 0;
                }
        }

        if(val == 0)
            cout << thr << " " << fiv << " " << sev << std::endl;
        else
            cout << -1 << endl;
    }
    
}