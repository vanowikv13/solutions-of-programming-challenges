//https://codeforces.com/problemset/problem/1433/A
#include <bits/stdc++.h>

using namespace std;

int main()
{
    size_t t;
    int num;
    cin >> t;
    for (size_t i = 0; i < t; i++)
    {
        cin >> num;
        int x1 = 1, x2 = 11, x3 = 111, x4 = 1111;
        int sum = 0;
        int digit = num % 10;
        for (; x4 <= 9999; x1++, x2 += 11, x3 += 111, x4 += 1111)
        {
            if ((num < x1 && digit == x1) || digit < x1) break;
            sum += 1;
            
            if ((num < x2 && digit == x1) || digit < x1) break;
            sum += 2;

            if ((num < x3 && digit == x1) || digit < x1) break;
            sum += 3;

            if ((num < x4 && digit == x1) || digit < x1) break;
            sum += 4;
        }
        cout << sum << endl;
    }
 }
