//https://codeforces.com/problemset/problem/231/A
#include <bits/stdc++.h>

using namespace std;

int main() {
    size_t n;
    cin >> n;
    int count = 0;
    for (size_t i = 0; i < n; i++)
    {
        short p, v, t;
        cin >> p >> v >> t;
        if(p + v + t >= 2)
            count++;
    }
    cout << count << endl;
    
}
