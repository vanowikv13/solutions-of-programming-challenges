//https://codeforces.com/blog/entry/84248
#include <bits/stdc++.h>

using namespace std;

int main() {
    size_t t;
    cin >> t;

    int a, b, c, d;

    for (size_t i = 0; i < t; i++)
    {
        cin >> a >> b >> c >> d;
        if(a + b > c + d)
            std::cout << a + b << std::endl;
        else
            std::cout << d + c << std::endl;
        
    }
}