//https://codeforces.com/problemset/problem/1445/A
#include <bits/stdc++.h>

using namespace std;

bool check(vector<int> a, vector<int> b, size_t n, int x) {
    std::reverse(a.begin(), a.end());

    for (size_t i = 0; i < n; i++)
        if(a[i] + b[i] > x)
            return false;

    return true;
}

int main() {

    size_t t,n;
    cin >> t;

    for (size_t i = 0; i < t; i++)
    {
        int x;
        cin >> n >> x;

        vector<int> a, b;
        a.resize(n); b.resize(n);
        for (size_t j = 0; j <  n; j++)
            cin >> a[j];

        for (size_t j = 0; j <  n; j++)
            cin >> b[j];

        if(check(a, b, n, x))
            cout << "YES" << endl;
        else
            cout << "NO" << endl;
        
    }
    
    return 0;
}
