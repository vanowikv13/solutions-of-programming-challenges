//https://codeforces.com/contest/1437/problem/D
#include <bits/stdc++.h>

using namespace std;

uint32_t shortestTreeSize(vector<uint32_t> vertex, size_t n) 
{
    if(vertex.size() == 1)
        return 0;

    vector<uint32_t> counter{1};
    int counterActuall = 0;
    for(size_t i = 1; i < vertex.size(); i++) {
        if(vertex[i-1] > vertex[i]) {
            if(counter.back() == 1) {
                counter.back() = 0;
                counter.push_back(counterActuall);
                counterActuall = 0;
            }
            else if(counter.back() > 1)
                counter.back()--;
        }
        counterActuall++;
    }
    if(counterActuall > 0)
        return counter.size();
    return counter.size() - 1;
}

int main() {
    size_t t, n;
    cin >> t;
    
    for(int i = 0; i < t; i++) {
        vector<uint32_t> vertex;
        vertex.reserve(n);
        uint32_t tmp;
        cin >> n;

        for (size_t j = 0; j < n; j++)
        {
            cin >> tmp;
            vertex.push_back(tmp);
        }
        cout << shortestTreeSize(vertex, n) << endl;
        vertex.clear();
    }
    return 0;
}