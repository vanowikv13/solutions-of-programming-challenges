#include <bits/stdc++.h>
#include <algorithm>

using namespace std;

const int INF = 1e9;

long absx(long x, long y) {
    if(x > y)
        return x - y;
    return y - x;
}

long minx(long x, long y) {
    return x > y ? y : x;
}

void solve()
{
    int n;
    scanf("%d", &n);
    vector<int> t(n);
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &t[i]);
        --t[i];
    }
    sort(t.begin(), t.end());
    
    vector<vector<int>> dp(n + 1, vector<int>(2 * n, INF));
    dp[0][0] = 0;
    for (size_t i = 0; i < n + 1; i++)
        for (size_t j = 0; j < 2 * n - 1; j++)
            if (dp[i][j] < INF)
            {
                if (i < n)
                    dp[i + 1][j + 1] = minx(dp[i + 1][j + 1], dp[i][j] + absx(t[i], j));
                dp[i][j + 1] = min(dp[i][j + 1], dp[i][j]);
            }
    printf("%d\n", dp[n][2 * n - 1]);
}

int main()
{
    int q;
    scanf("%d", &q);
    for (size_t i = 0; i < q; i++)
        solve();
}