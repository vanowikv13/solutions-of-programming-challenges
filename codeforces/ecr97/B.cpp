//https://codeforces.com/contest/1437/problem/B
#include <bits/stdc++.h>

using namespace std;

int countMoves(string s, size_t r) {
    int ones = 0, zeros = 0;
    for (size_t i = 0; i < r; i++)
    {
        if(s[i] == '1' && s[i - 1] == '1')
            ones++;
        else if(s[i] == '0' && s[i - 1] == '0')
            zeros++;
    }
    return ((ones > zeros) ? ones : zeros);
}

int main() {
    size_t t, n;
    string s;
    cin >> t;

    for (size_t k = 0; k < t; k++)
    {
        cin >> n;
        cin >> s;
        cout << countMoves(s, n) << endl;
    }
    
}