//https://codeforces.com/contest/1437/problem/C
#include <bits/stdc++.h>

using namespace std;

long absx(long x, long y) {
    if(x > y)
        return x - y;
    return y - x;
}

void countTimes(map<long, long> &ct, vector<long> &t) {
    for (size_t i = 0; i < t.size(); i++)
    {
        if(ct.find(t[i]) == ct.end())
            ct[t[i]] = 1;
        else
            ct[t[i]]++;
    }
}

long countTimeRanges(size_t start, size_t actTime, size_t k, vector<long> &t) {
    long sum = 0, min_k = k, min_sum = 0;
    bool first = false;
    for(size_t i_ = actTime; i_ <= k; i_++) {
        for(size_t j = start, time = i_; j <= t.size(); j++, time++)
            sum += absx(t[j], time);

        if(!first) {
            min_k = i_;
            min_sum = sum;
            first = true;
        }

        if(sum < min_sum) {
            min_k = i_;
            min_sum = sum;
        }

        if(sum == min_sum && min_k > i_)
            min_k = i_;
        sum = 0;
    }
    return min_k;
}

long countTime(vector<long> &t) {
    sort(t.begin(), t.end());
    map<long, long> ct;
    countTimes(ct, t);

    long uVal = 0;

    size_t time = 1;
    long lastCheck = 0;
    for(size_t i = 0; i < t.size(); i++) {
        if(t[i] != lastCheck && t[i] > time) {
                size_t k = t[i] - static_cast<long>(((ct[t[i]]) / 2));
                if(k > time) {
                    long k_ = countTimeRanges(i, time, k, t);
                    time = k_;
                }
                lastCheck = t[i];
        }
        uVal += absx(time, t[i]);
        time+=1;
    }
    return uVal;
}

int main() {
    size_t q, n;
    long ti;
    cin >> q;

    for (size_t i = 0; i < q; i++)
    {
        cin >> n;
        vector<long> v;
        v.reserve(n);
        for (size_t j = 0; j < n; j++)
        {
            cin >> ti;
            v.push_back(ti);
        }
        cout << countTime(v) << endl;
        v.clear();
    }
    
}