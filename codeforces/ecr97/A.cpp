//https://codeforces.com/contest/1437/problem/A
//Gives wrong answert on test 2
#include <bits/stdc++.h>

using namespace std;

bool findA(uint64_t l, uint64_t r) {
    for(uint64_t i = (l / 2) + 1; i < r * 2; i++) {
        if(i >= l &&  i <= r)
            i = r +1;
        uint64_t tmpl = l - static_cast<uint64_t>(l / i) * i;

        uint64_t tmpr = r - static_cast<uint64_t>(r / i) * i;
        if((static_cast<float>(tmpl % i) >= i / 2 && tmpl != 0 && tmpl != 1) && 
        (static_cast<float>(tmpr % i) >= i / 2 && tmpr != 0 && tmpr != 1))
            return true;
    }
    return false;
}

bool checkIfBuyMore(uint64_t &l, uint64_t &r) {
    return (findA(l, r));
}

int main() {
    size_t t;
    cin >> t;
    
    long x = pow(10, 9);
    for (size_t k = 0; k < t; k++)
    {
        uint64_t l, r;
        cin >> l >> r;
        if(checkIfBuyMore(l, r)) {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }
    
}