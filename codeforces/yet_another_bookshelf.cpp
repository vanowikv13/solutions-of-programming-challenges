//https://codeforces.com/problemset/problem/1433/B
#include <bits/stdc++.h>

using namespace std;

int main() {
    size_t t, n;
    cin >> t;
    for (size_t i = 0; i < t; i++)
    {
        cin >> n;
        
        short num;
        bool firstOne = false;
        int moves = 0;
        int tmp = 0;

        for (size_t j = 0; j < n; j++)
        {
            cin >> num;
            if(num == 1) {
                if(firstOne) {
                    moves += tmp;
                }
                
                tmp = 0;
                firstOne = true;
            } else
                tmp += 1;
        }
        cout << moves << endl;
        
    }
    return 0;
}
