//https://codeforces.com/problemset/problem/1428/A
#include <bits/stdc++.h>

using namespace std;

int main() {
    size_t t;
    cin >> t;

    long long x1, y1, x2, y2;
    for (size_t i = 0; i < t; i++)
    {
        cin >> x1 >> y1 >> x2 >> y2;
        long long seconds = 0;
        long long difX = abs(x2 - x1);
        long long difY = abs(y2 - y1);

        if(x2 == x1) {
            seconds = difY;
        } else if(y2 == y1) {
            seconds = difX;
        }
        else {
            seconds = 2 + difY + difX;
        }

        cout << seconds << endl;

    }
    
    return 0;
}