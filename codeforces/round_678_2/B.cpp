#include <bits/stdc++.h>

using namespace std;

bool isPrime(uint64_t num) {
    if(num == 1)
        return false;
    if(num == 2)
        return true;
    if(num %2 == 0)
        return false;
    for(uint64_t i = 3; i < num; i+=2)
        if(num % i == 0)
            return false;
    return true;
}

uint64_t findNextToAdd(uint64_t sum) {
    for(int i = sum+1; i < pow(10, 5); i++) {
        if(isPrime(i))
            if(!isPrime(i - sum))
                return (i - sum);
    }
    return 1;
}

int main() {
    size_t t, n;
    cin >> t;
    for (size_t k = 0; k < t; k++)
    {
        cin >> n;
        uint64_t** square = new uint64_t*[n];

        for (size_t i = 0; i < n; i++)
        {
            square[i] = new uint64_t[n];
            for (size_t j = 0; j < n; j++)
                square[i][j] = 1;
        }

        uint64_t tmp = findNextToAdd(n-1);
        for (size_t i = 0; i < n-1; i++) {
            square[i][n-1] = tmp;
            square[n-1][i] = tmp;
        }
        
        square[n-1][n-1] = findNextToAdd(tmp * (n -1));
        
        for(size_t i = 0; i < n; i++) {
            for (size_t j = 0; j < n;j++)
            {
                cout << square[i][j] << " ";
            }
            cout << endl;
            delete square[i];
        }
        delete [] square;
    }
}
