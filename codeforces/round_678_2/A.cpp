//https://codeforces.com/contest/1436/problem/A
#include <bits/stdc++.h>

using namespace std;

int main() {
    size_t t, n;
    long long m;
    cin >> t;
    for (size_t i = 0; i < t; i++)
    {
        cin >> n >> m;
        long long sum = 0, ai;
        for (size_t j = 0; j < n; j++)
        {
            cin >> ai;
            sum += ai;
        }
        
        if(sum == m) {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }
    
}
