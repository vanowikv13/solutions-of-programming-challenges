///https://codeforces.com/problemset/problem/71/A
#include <bits/stdc++.h>

using namespace std;

int main() {
    size_t w;
    cin >> w;
    std::string s;
    for (size_t i = 0; i < w; i++)
    {
        cin >> s;
        if(s.size() > 10) {
            cout << s[0] + to_string(s.size()-2) + s[s.size()-1] << endl;
        } else {
            cout << s << endl;
        }
    }
    
}
