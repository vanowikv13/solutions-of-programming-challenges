//https://codeforces.com/problemset/problem/158/A
/*
Lesson from this: remember to always check the max size of variable (if short is not to small etc.)
*/
#include <bits/stdc++.h>

using namespace std;

//it doesn't fully work :( but the error is on 8189 case
int testPhiranias(vector<int> &p, unsigned int n, int max_p) {
    if(n == 1)
        return 1;

    int p_index = -2;
    for(unsigned int i = 0; i < n; i++) {
        if(p[i] == max_p) {
            if(i > 0 && p[i - 1] != max_p)
                p_index = i;
            if(i < n - 1 && p[i + 1] != max_p)
                p_index = i;
        }
    }
    return p_index + 1;
}

int main() {    
    unsigned int aquariums, piranhas;
    vector<int> p;
    cin>>aquariums;
    for (unsigned int i = 0; i < aquariums; i++)
    {
        cin>>piranhas;
        p.resize(piranhas);
        int value = 0;
        int max_v = 0;
        for (unsigned int j = 0; j < piranhas; j++)
        {
            cin >> value;
            p[j] = value;
            if(value > max_v)
                max_v = value;
        }
        cout << testPhiranias(p, piranhas, max_v) << endl;
    }    
}
