//https://codeforces.com/problemset/problem/1430/B
#include <bits/stdc++.h>

using namespace std;

int main() {
    size_t t, k, n;
    cin >> t;

    for (size_t i = 0; i < t; i++)
    {
        cin >> n >> k;
        multiset<long long> a;
        long long num;

        for (size_t j = 0; j < n; j++)
        {
            cin >> num;
            a.insert(num);
        }

        size_t m = 0;
        long long sum = 0;

        for(auto it = a.rbegin(); it != a.rend(); it++) {
            if(m == k + 1)
                break;
            sum += *it;
            m++;
        }
        cout << sum << endl;
    }
}
