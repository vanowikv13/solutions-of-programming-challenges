https://codeforces.com/problemset/problem/158/A
#include <bits/stdc++.h>

using namespace std;

int main() {
    size_t n, place, qualified = 0;
    cin >> n >> place;
    
    size_t score, last = 0;
    for(int i = 0; i < n; i++) { 
        cin >> score;
        if(score > 0) {
            if(i+1 <= place || last == score)  {
                qualified++;
                last = score;
            }
        }

    }
    cout << qualified << endl;
}
